const React = require('react');
const ReactDOM = require('react-dom');
import axios from 'axios';

class App extends React.Component {

    constructor(props) {
        super(props);
    }

    runHelloWorldScript() {
        axios.get('http://localhost:8080/script/helloWorld', {
        })
            .then(function (response) {
                console.log("response: ", response);
            })
            .catch(function (error) {
                console.log("error: ", error);
            });
    }

    render() {
        return (
            <div>
                <div>
                    <button onClick={this.runHelloWorldScript}>run hello world script</button>
                    {/* more scripts
                    <button onClick={this..................}>......................</button>
                    */}
                </div>
            </div>

        )
    }
}
ReactDOM.render(
    <App/>,
    document.getElementById('react')
)