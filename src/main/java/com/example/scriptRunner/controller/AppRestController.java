package com.example.scriptRunner.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/script")
public class AppRestController {

    @RequestMapping(method = RequestMethod.GET, value = "/helloWorld")
    ResponseEntity<?> script(){
        System.out.println("script launched");
        ProcessBuilder pb = new ProcessBuilder("./src/main/resources/static/script.sh", "myArg1", "myArg2");
        pb.inheritIO();
        try {
            pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
